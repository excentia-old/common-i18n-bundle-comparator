/*
 * I18n Bundle Comparator
 * Copyright (C) 2014 excentia
 * contact@excentia.es
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02
 */
package es.excentia.common.i18n.bundle.comparator.exception;

/**
 * Properties Exception
 */
public class BundleException extends Exception {

  private static final long serialVersionUID = -5565306643720465302L;

  /**
   * Constructor
   * 
   * @param message
   */
  public BundleException(String message) {
    super(message);
  }

  /**
   * Constructor
   * 
   * @param cause
   */
  public BundleException(Throwable cause) {
    super(cause);
  }

  /**
   * Constructor
   * 
   * @param message
   * @param cause
   */
  public BundleException(String message, Throwable cause) {
    super(message, cause);
  }
}