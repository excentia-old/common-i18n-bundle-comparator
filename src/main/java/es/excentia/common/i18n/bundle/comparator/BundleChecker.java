/*
 * I18n Bundle Comparator
 * Copyright (C) 2014 excentia
 * contact@excentia.es
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02
 */
package es.excentia.common.i18n.bundle.comparator;

import java.io.File;
import java.io.InputStream;
import java.util.Collection;
import java.util.Map;
import java.util.SortedMap;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Main application
 */
public final class BundleChecker {

  private static final Logger LOG = LoggerFactory.getLogger(BundleChecker.class);

  /**
   * Constructor
   */
  private BundleChecker() {
  }

  /**
   * @param args
   */
  public static void main(String[] args) {
    compare(args);
  }
  
  public static boolean compare (String[] args) {
    boolean success = true;
   
    if (args == null || args.length < 1) {
      LOG.error("Bundles directory path is not defined.");
      LOG.error("Usage: java -jar common-i18n-bundle-comparator.jar path/to/bundles/directory [defaultBundleFile]");
      success = false;
    } else {
      // Folder
      String bundleFolder = args[0];
      String defaultBundleName = null;
      
      LOG.debug("args.length: " + args.length);
      
      if (args.length >= 2){
        defaultBundleName = args[1];
      }
      
      // Convert to file
      File folder = new File(bundleFolder);

      if (folder.isDirectory()) {
        int missingCount = 0;
        InputStream bundleInputStream = null;
        InputStream defaultBundleInputStream = null;

        LOG.debug("Bundle folder: " + bundleFolder);
        LOG.debug("Bundle file: " + defaultBundleName);

        try {
          // Operations
          BundleOperations operations = new BundleOperations(bundleFolder);

          // Collect all bundles
          Collection<File> bundles = FileUtils.listFiles(folder, new String[] { "properties" }, false);

          if ( !bundles.isEmpty()) {
            LOG.info("Found " + bundles.size() + " bundle files.");

            for (File bundle : bundles) {
              LOG.info("---> " + bundle.getName());
            }
          }

          // Default bundle file
          if (StringUtils.isBlank(defaultBundleName)){
            defaultBundleName = operations.extractDefaultBundleName();
          }

          LOG.info("Default bundle file: " + defaultBundleName);
          
          for (File bundle : bundles) {
            // Read file
            bundleInputStream = operations.getBundleFileInputStream(bundle.getName());

            // Find the default bundle which the provided one should be compared to
            defaultBundleInputStream = operations.getDefaultBundleFileInputStream(defaultBundleName);

            // Search for missing keys
            SortedMap<String, String> missingKeys = operations.retrieveMissingTranslations(bundleInputStream, defaultBundleInputStream);

            if ( !missingKeys.isEmpty()) {
              LOG.info(missingKeys.size() + " missing keys for: " + bundle.getName());

              // Count missing keys
              missingCount += missingKeys.size();
              success = false;
            }

            for (Map.Entry<String, String> entry : missingKeys.entrySet()) {
              LOG.info("---> " + entry.getKey());
            }
          }

          LOG.info("Total missing bundle keys: " + missingCount);
          
        } catch (Exception exception) {
          LOG.error("An error occured while reading the bundles: " + exception.getMessage());
          LOG.debug("Details:", exception);
          success = false;
        } finally {
          IOUtils.closeQuietly(bundleInputStream);
          IOUtils.closeQuietly(defaultBundleInputStream);
        }
      }
      // Is not a folder
      else {
        LOG.error(bundleFolder + " is not a folder.");
        success = false;
      }
    }
    LOG.debug("SUCCESS.................. " + success);
    return success;
    
  }
}