/*
 * I18n Bundle Comparator
 * Copyright (C) 2014 excentia
 * contact@excentia.es
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02
 */
package es.excentia.common.i18n.bundle.comparator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Map;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.commons.io.FileUtils;

import es.excentia.common.i18n.bundle.comparator.exception.BundleException;

/**
 * Class to operate with bundle files
 */
public class BundleOperations {

  private final String bundleFolder;

  /**
   * Constructor
   * 
   * @param bundleFolder
   * @throws BundleExceptionTest
   */
  public BundleOperations(String bundleFolder) {
    // Check format
    if (bundleFolder.endsWith("/")) {
      this.bundleFolder = bundleFolder;
    }
    // Format folder
    else {
      this.bundleFolder = bundleFolder.concat("/");
    }
  }

  /**
   * @return the bundle folder
   */
  public final String getBundleFolder() {
    return bundleFolder;
  }

  /**
   * @return default bundle name
   * @throws BundleException
   */
  public final String extractDefaultBundleName() throws BundleException {
    String defaultBundle = null;

    // Convert to file
    File folder = new File(bundleFolder);
    File firstBundle = null;

    firstBundle = fileIsDirectoryAndBundlesNotEmpty(folder);

    if (firstBundle != null) { // El path es un directorio y contiene ficheros de internacionalización
      // Read first bundle

      defaultBundle = extractFirstBundleName(firstBundle.getPath());

    }

    return defaultBundle;
  }

  private File fileIsDirectoryAndBundlesNotEmpty(File folder) throws BundleException {
    File firstBundle = null;

    if (folder.isDirectory()) {
      Collection<File> bundles = FileUtils.listFiles(folder, new String[] { "properties" }, false);
      if (bundles.isEmpty()) {
        throw new BundleException(bundleFolder + " folder does not contains any bundle file.");
      } else {
        firstBundle = bundles.iterator().next();
      }
    } else {
      throw new BundleException(bundleFolder + " is not a folder.");
    }

    return firstBundle;
  }

  /**
   * @param path
   * @return properties file name
   */
  public String extractFirstBundleName(String path) {
    String myFirstBundleName = path.substring(path.lastIndexOf(File.separatorChar) + 1);
    String fileName = myFirstBundleName.split("\\.")[0];
    String defaultBundle;

    int index = fileName.indexOf('_');

    // Default bundle
    if (index == -1) {
      defaultBundle = String.valueOf(fileName);
    }
    // Specific bundle
    else {
      defaultBundle = fileName.substring(0, index);
    }

    return defaultBundle + ".properties";
  }

  /**
   * @param defaultBundleName
   * @return default bundle InputStream
   * @throws BundleException
   * @throws FileNotFoundException
   */
  public final InputStream getDefaultBundleFileInputStream(String defaultBundleName) throws BundleException, FileNotFoundException {
    File defaultBundleFile = new File(bundleFolder + defaultBundleName);
    InputStream bundleStream = new FileInputStream(defaultBundleFile);

    return bundleStream;
  }

  /**
   * @return InputStream of the bundle
   * @throws FileNotFoundException
   */
  public final InputStream getBundleFileInputStream(String bundleName) throws FileNotFoundException {
    File defaultBundleFile = new File(bundleFolder + bundleName);
    InputStream bundleStream = new FileInputStream(defaultBundleFile);

    return bundleStream;
  }

  /**
   * @param inputStream
   * @return properties loaded
   * @throws IOException
   */
  private Properties loadProperties(InputStream inputStream) throws IOException {
    Properties props = new Properties();
    props.load(inputStream);

    return props;
  }

  /**
   * @param bundle
   * @param referenceBundle
   * @return missing keys
   * @throws IOException
   */
  public final SortedMap<String, String> retrieveMissingTranslations(InputStream bundle, InputStream referenceBundle) throws IOException {
    SortedMap<String, String> missingKeys = new TreeMap<String, String>();

    Properties bundleProps = loadProperties(bundle);
    Properties referenceProperties = loadProperties(referenceBundle);

    for (Map.Entry<Object, Object> entry : referenceProperties.entrySet()) {
      String key = (String) entry.getKey();

      if ( !bundleProps.containsKey(key)) {
        missingKeys.put(key, (String) entry.getValue());
      }
    }

    return missingKeys;
  }
}