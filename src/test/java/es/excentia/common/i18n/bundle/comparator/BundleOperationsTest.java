/*
 * I18n Bundle Comparator
 * Copyright (C) 2014 excentia
 * contact@excentia.es
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02
 */
package es.excentia.common.i18n.bundle.comparator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

import java.io.InputStream;
import java.net.URL;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import es.excentia.common.i18n.bundle.comparator.exception.BundleException;

public class BundleOperationsTest {

  private static String DEFAULT_BUNDLE_NAME = "sample.properties";
  private static String DEFAULT_BUNDLE_NAME_ES = "sample_es.properties";
  private static String BUNDLE_FOLDER = "Bundle folder";
  private static String BUNDLE_NAME = "Bundle name";
  private static String BUNDLE_STREAM = "Bundle stream";
  private static String DUMMY_FOLDER = "folder/";
  private static String MISSING_KEY = "Missing key";

  private URL folderName;
  BundleOperations operations;

  @Before
  public void setUp() {
    this.folderName = getClass().getResource("/i18n/");
    this.operations = new BundleOperations(folderName.getPath());
  }

  @Test
  public void testCreationSuccess() {
    assertEquals(BUNDLE_FOLDER, testCreation(DUMMY_FOLDER), true); 
  }

  @Test
  public void testCreationWithouSlashSuccess() {
    assertEquals(BUNDLE_FOLDER, testCreation("folder"), true); 
  }

  private boolean testCreation(String folder) {
    return DUMMY_FOLDER.equalsIgnoreCase((new BundleOperations(folder)).getBundleFolder());
  }

  @Test(expected = BundleException.class)
  public void testExtractDefaultBundleNameFolderNotExists() throws BundleException {
    BundleOperations operations = new BundleOperations("");
    operations.extractDefaultBundleName();
  }

  @Test
  public void testExtractDefaultBundleNameSuccess() throws BundleException {
    String bundleName = operations.extractDefaultBundleName();
    assertEquals(BUNDLE_NAME, bundleName, DEFAULT_BUNDLE_NAME);
  }

  @Test
  public void testExtractDefaultBundleNameWithExtension() {
    assertEquals(BUNDLE_NAME, testBundleNameExtraction(DEFAULT_BUNDLE_NAME), DEFAULT_BUNDLE_NAME);
  }

  @Test
  public void testExtractDefaultBundleNameWithoutExtension() {
    assertEquals(BUNDLE_NAME, testBundleNameExtraction("sample"), DEFAULT_BUNDLE_NAME);
  }

  @Test
  public void testExtractDefaultBundleStreamSuccess() {
    InputStream bundleStream = null;

    try {
      String defaultBundleName = DEFAULT_BUNDLE_NAME;
      bundleStream = operations.getDefaultBundleFileInputStream(defaultBundleName);
      assertNotNull(BUNDLE_STREAM, bundleStream);
    } catch (Exception exception) {
      Assert.fail();
    } finally {
      IOUtils.closeQuietly(bundleStream);
    }
  }

  @Test
  public void testBundleStreamSuccess() {
    InputStream bundleStream = null;

    try {
      bundleStream = operations.getBundleFileInputStream(DEFAULT_BUNDLE_NAME_ES);
      assertNotNull(BUNDLE_STREAM, bundleStream);
    } catch (Exception exception) {
      Assert.fail();
    } finally {
      IOUtils.closeQuietly(bundleStream);
    }
  }

  @Test
  public void testRetrieveMissingTranslationsSuccess() {
    InputStream defaultBundleStream = null;
    InputStream bundleStream = null;
    String defaultBundleName = DEFAULT_BUNDLE_NAME;

    try {
      defaultBundleStream = operations.getDefaultBundleFileInputStream(defaultBundleName);
      bundleStream = operations.getBundleFileInputStream(DEFAULT_BUNDLE_NAME_ES);

      Map<String, String> missingKeys = operations.retrieveMissingTranslations(bundleStream, defaultBundleStream);

      assertSame("2 missing keys", missingKeys.size(), 2);
      assertNotNull(MISSING_KEY, missingKeys.get("my.third.property"));
      assertNotNull(MISSING_KEY, missingKeys.get("my.fourth.property"));

    } catch (Exception exception) {
      Assert.fail();
    } finally {
      IOUtils.closeQuietly(defaultBundleStream);
      IOUtils.closeQuietly(bundleStream);
    }
  }

  private String testBundleNameExtraction(String bundleName) {
    return operations.extractFirstBundleName(bundleName);
  }

}