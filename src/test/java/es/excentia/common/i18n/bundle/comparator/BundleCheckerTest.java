/*
 * I18n Bundle Comparator
 * Copyright (C) 2014 excentia
 * contact@excentia.es
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02
 */
package es.excentia.common.i18n.bundle.comparator;

import org.junit.Assert;
import org.junit.Test;

public class BundleCheckerTest {

  @Test
  public void testExecutionWithNullArgument() {
    try {
      BundleChecker.main(null);
    } catch (Exception exception) {
      Assert.fail();
    }
  }

  @Test
  public void testExecutionWithEmptyArgument() {
    try {
      BundleChecker.main(new String[0]);
    } catch (Exception exception) {
      Assert.fail();
    }
  }

  @Test
  public void testExecutionFolderNoExists() {
    try {
      String[] args = { "fakePath" };
      BundleChecker.main(args);

    } catch (Exception exception) {
      Assert.fail();
    }
  }

  @Test
  public void testExecutionFolderWithNoBundles() {
    try {
      String[] args = { "src/" };
      BundleChecker.main(args);

    } catch (Exception exception) {
      Assert.fail();
    }
  }

  @Test
  public void testExecutionSuccess() {
    try {
      String[] args = { "src/test/resources/i18n/" };
      BundleChecker.main(args);

    } catch (Exception exception) {
      Assert.fail();
    }
  }
}