# I18n Bundle Comparator
With this application you can find the missing keys of the i18n bundle files of a directory.

## Usage
```shell
java -jar common-i18n-bundle-comparator.jar path/to/bundles/directory
```

Download last version from [here](https://bitbucket.org/excentia/common-i18n-bundle-comparator/downloads/common-i18n-bundle-comparator-1.0.jar).